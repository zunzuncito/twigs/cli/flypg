
import path from "path"
import fs from "fs"

import readline from 'readline'
import { Writable } from 'stream'

import crypto from "crypto"


import pg from 'pg'
const { Pool, Client } = pg;

import CLIUtil from 'zunzun/flyutil/cli.mjs'


import PGAura from 'zunzun/pg-aura/index.mjs'

class SuperPG {
  static async connect(susr, spwd, cfg) {
    try {
      let this_class = new SuperPG(susr, spwd, cfg);

      await this_class.client.connect();

      this_class.client.on('error', (err, client) => {
        console.error('Unexpected error on idle client', err)
        process.exit(-1)
      })
      
      return this_class;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
  
  constructor(susr, spwd, cfg) {
    if (!cfg) cfg = {};
    this.host_addr = cfg.host_addr || "127.0.0.1";
    this.host_port = cfg.host_port || 5432; 
    this.client = new Client({
      user: susr,
      password: spwd,
      host: this.host_addr,
      database: 'postgres',
      port: this.host_port 
    });
  }
  
  async create_database(name, susr, spwd) {
    try {
      console.debug("Create db", name);
      let exists = await this.client.query("SELECT datname FROM pg_catalog.pg_database WHERE lower(datname) = lower('"+name+"')");
      if (exists.rowCount == 0) {
        await this.client.query("CREATE DATABASE "+name+";");
   //     await this.client.query("REVOKE connect ON DATABASE "+name+" FROM PUBLIC;");
      }

      let tmp_client = new Client({
        user: susr,
        password: spwd,
        host: this.host_addr,
        database: name,
        port: this.host_port
      });
      await tmp_client.connect();

      tmp_client.on('error', (err, client) => {
        console.error('Unexpected error on idle client', err)
        process.exit(-1)
      });

      await tmp_client.query('CREATE EXTENSION IF NOT EXISTS "pgcrypto";');

      tmp_client.end().catch(err => console.error('error during disconnection', err.stack))
    } catch (e) {
      console.error(e.stack);
    }
  }


  async create_user(usr, pwd) {
    try {
      console.debug("Create user", usr);
      await this.client.query("CREATE USER "+usr+" WITH PASSWORD '"+pwd+"';");
    } catch (e) {
      console.error(e.stack);
    }
  }

  async grant_privileges(db, usr) {
    try {
      let qstr = "GRANT ALL PRIVILEGES ON DATABASE "+db+" to "+usr+";"
      qstr += `ALTER DATABASE ${db} OWNER TO ${usr};`
      await this.client.query(qstr);
    } catch (e) {
      console.error(e.stack);
    }
  }

  disconnect() {
    this.client.end().catch(err => console.error('error during disconnection', err.stack))
  }
}

export default class FlyPG {
  static async run(config) {
    try {
      let testing = false;
      for (let arg of process.argv) {
        if (arg == "--testing") testing = true;
      }


      if (process.argv[3] == "export") {
        const pg_cfg = JSON.parse(fs.readFileSync(path.resolve(
          process.cwd(), '.pg-env.json'
        )));
        const aura = await PGAura.connect({
          database: pg_cfg.db_name,
          user: pg_cfg.db_user,
          pass: pg_cfg.db_pwd
        });
        
        const tables = await aura.query("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public' AND table_type = 'BASE TABLE';");

        const prioritize = [];
        const check_relations = async (table_name) => {
          const relations = await aura.query(
`SELECT
  conname AS constraint_name,
  conrelid::regclass AS table_name,
  a.attname AS column_name,
  confrelid::regclass AS referenced_table,
  b.attname AS referenced_column
FROM
  pg_constraint c
JOIN
  pg_attribute a ON a.attnum = ANY(conkey) AND a.attrelid = conrelid
JOIN
  pg_attribute b ON b.attnum = ANY(confkey) AND b.attrelid = confrelid
WHERE
  conrelid = '${table_name}'::regclass;`);

          if (relations.rows.length > 0) {
            for (let row of relations.rows) {
              await check_relations(row.referenced_table);
            }
          }

          if (!prioritize.includes(table_name)) prioritize.push(table_name);
        }



        for (let table of tables.rows) {


          console.log(table.table_name);
          await check_relations(table.table_name);
        }

          console.log(prioritize);

        const tables_json = []
        for (let table_name of prioritize) {


          console.log(table_name);
          tables_json.push({
            name: table_name,
            rows: (await aura.query(`select * from ${table_name};`)).rows
          });
        }

        const out_path = path.resolve(process.cwd(), "db.json");
        fs.writeFileSync(out_path, JSON.stringify(tables_json, null, 2));

        aura.client.end();
      } else if (process.argv[3] == "import") {
        const pg_cfg = JSON.parse(fs.readFileSync(path.resolve(
          process.cwd(), '.pg-env.json'
        )));
        const aura = await PGAura.connect({
          database: pg_cfg.db_name,
          user: pg_cfg.db_user,
          pass: pg_cfg.db_pwd
        });
        

        const out_path = path.resolve(process.cwd(), "db.json");
        const tables_json = JSON.parse(fs.readFileSync(out_path, 'utf8'));
        for (let table of tables_json) {
          const table_name = table.name
          for (let row of table.rows) {
            const cols = [];
            const exps = [];
            const vals = [];

            let expi = 1;
            for (let col in row) {
              cols.push(col);
              exps.push(`$${expi}`);
              vals.push(row[col])
              expi++;
            }

//            console.log( `INSERT INTO ${table_name} (${cols.join(', ')}) VALUES (${exps.join(', ')})`);

            if (expi > 1) {
              try {
                await aura.query({
                  text: `INSERT INTO ${table_name} (${cols.join(', ')}) VALUES (${exps.join(', ')})`,
                  values: vals, // Replace with actual values from JSON
                });
              } catch (e) {
                console.log("Values:", vals);
                console.error(e.stack);
              }
            }
          }
        }

        aura.client.end();
      } else if (process.argv[3] == "init") {

        const cwd = process.cwd()
        const env = {};
  
        let out_path = undefined;
        let env_path = undefined;

        let db_user = undefined;
        let db_pwd = undefined;
        if (!testing) {
          let rl = CLIUtil.InputReader();

          const twig_json_path = path.join(cwd, "twig.json");
          let default_name = "new_app";
          if (fs.existsSync(twig_json_path)) {
            const twig_json = JSON.parse(fs.readFileSync(twig_json_path, 'utf8'));
            if (twig_json.name) default_name = twig_json.name;
          }

          let env_name = await rl.questionSync(`App name[${default_name}]: `);
          if (!env_name) env_name = default_name;
          
          console.log("");
          console.log("\x1b[36m>>> Authenticate with you PostgresSQL admin account <<<\x1b[0m");

          db_user = await rl.questionSync('User[postgres]: ');
          if (!db_user) db_user = "postgres";

          db_pwd = await rl.questionSync('Password: ', true);

          out_path = await rl.questionSync('Output[.pg-env.json]: ');
          rl.close();
          if (!out_path) out_path = ".pg-env.json"
          env_path = path.resolve(cwd, out_path);

          env.db_user = env.db_name = env_name.replace(/\s+/g, '').replace('.', '_').replace('-', '_').toLowerCase();
          env.db_pwd = crypto.randomBytes(10).toString('hex');
        } else {
          env.db_user = env.db_name = "zunzun_flytest";
          env.db_pwd = "0123456789";
          out_path = ".pg-env.json"
          env_path = path.resolve(cwd, out_path);

          console.log("env_path", env_path);
          if (fs.existsSync(env_path)) {
            try {
              let env = JSON.parse(fs.readFileSync(env_path, "utf8"));
              if (
                env.db_user == "zunzun_flytest" &&
                env.db_name == "zunzun_flytest" &&
                env.db_pwd == "0123456789"
              ) {
                console.log("Test environment ready! Skiping database setup...");
                return;
              }
            } catch (e) {
              console.error(e.stack);
              return;
            }
          } else {
            db_user = "postgres";
            db_pwd = "123456";
          }
        }

        let super_aura = await SuperPG.connect(db_user, db_pwd);
        await super_aura.create_database(env.db_name, db_user, db_pwd);
        await super_aura.create_user(env.db_name, env.db_pwd);
        await super_aura.grant_privileges(env.db_name, env.db_name);
        super_aura.disconnect();

        console.log("");
        console.log("Database and user`"+env.db_name+"` have been created.");


        fs.writeFileSync(env_path, JSON.stringify(env), 'utf8');
        console.log("Configuration saved to:", env_path);
      } else {
        console.error(new Error(`Invalid command line parameter ${process.argv[3]}`));
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

}
